<!DOCTYPE html>
<html>
<head>
	<title>Gamintojai</title>
	<?php include 'headerlink.html'; ?>
	<link rel="stylesheet" type="text/css" href="css/stylebrand.css">
	<script src="js/scriptbrand.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
	<?php include 'header.html'; ?>
	<?php 

	$brandas1 = null;
	$brandas2 = null;
	$brandas3 = null;
	$t1priskirtas = false;
	$brandai = array("bmw", "honda", "suzuki");
	if(isset($_GET['tipas']) && isset($_GET['brand']))
	{
		$tipas = $_GET['tipas'];
		$brandas = $_GET['brand'];

	}
	else
	{
		$tipas = 'standart' ;
		$brandas = 'bmw';

	}
	for ($i =0 ; $i<3 ; $i++ ) {
		if($brandas == $brandai[ $i ])
		{
			$brandas1 = $brandai[ $i ];
			$t1priskirtas = true;
			
		}
		else
		{
			$brandas2 = $brandai[$i];
			$brandas3 = $brandai[(($i+1)>2?0:($i+1))];
			if($t1priskirtas)$i=3;
			
		}
	}
	if($brandas1=="")$brandas1=$brandai[1];
	if($tipas=="")$tipas="standart";
// echo $brandas1.$tipas;

	?>

	<br>
	<div >
		<section>
			<!-- <div id="pav1" style="background: url(images/<?php echo $brandas1.$tipas ?>.jpg);background-size: 700px 500px;background-repeat: repeat;"> -->
				<img id="pav1" src="images/<?php echo $brandas1.$tipas ?>.jpg">
				<p id="pavp1"><?php include "/txt/".$brandas1.$tipas.".txt" ;?></p>
			<!-- </div> -->
		</section>

		<script type="text/javascript">
			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {

				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     56],
					['Honda',      32],
					['Suzuki',    12]
					]);

				var options = {
					title: '2015 metu motociklu pardavimai'
				};

				var chart = new google.visualization.PieChart(document.getElementById('piechart'));
				chart.draw(data, options);
				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     12],
					['Honda',      76],
					['Suzuki',    43]
					]);

				var options = {
					title: '2016 metu motociklu pardavimai'
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
				chart.draw(data, options);
				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     40],
					['Honda',      22],
					['Suzuki',    15]
					]);

				var options = {
					title: '2017 metu motociklu pardavimai'
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart3'));
				chart.draw(data, options);
			}
		</script>

		<div class="graf">
		<ul class="row">
				<li class="col s12 m4 l4">
					<div id="piechart" style="width: 450px; height: 250px;"></div>
				</li>
				<li class="col s12 m4 l4">
					<div id="piechart2" style="width: 450px; height: 250px;"></div>
				</li>
				<li class="col s12 m4 l4">
					<div id="piechart3" style="width: 450px; height: 250px;"></div>
				</li>
			</ul>
		</div>
		<br>
		<ul id="ulas" class="row">
			<li class="col s10 l6">
				<ul class="row">
					<li class="col s10 l6">
						<img id="id1" class="pav2" src="images/<?php echo $brandas2.$tipas ;?>.jpg">
					</li>
					<li class="col s10 l6">
						<p id="pavp2" class="truncate"><?php include "/txt/".$brandas2.$tipas.".txt" ;?></p>
					</li>
				</ul>
			</li>
			<li class="col s10 l6">
			<ul class="row">
					<li class="col s10 l6">
						<img id="id2" class="pav2" src="images/<?php echo $brandas3.$tipas ; ?>.jpg">
					</li>
					<li class="col s10 l6">
						<p id="pavp3" class="truncate"><?php include "/txt/".$brandas3.$tipas.".txt" ;?></p>
					</li>
				</ul>
			</li>

		</ul>

	</div>

	<br>
	<div id="footeris">
		<br>

		<?php 
		include 'footer.html';
		?>
	</div>
</body>
</html>